---------------------------------------Requirements-----------------------
Data Base: MongoDB
Programing Language: Node
---------------------------------------Use Mode---------------------------
1-Installer dependencies (npm i)
2-Installer Postman or any tool for API RESTFul
3-Functionality
    3.1-Get all planeta(Ex. http://localhost:3000/api/planets)
    3.2-Find by name( http://localhost:3000/api/planets/name/parameter_name)
    3.3-Find by id( http://localhost:3000/api/planets/id)
    3.4-Create  Planet.Post call  to: http://localhost:3000/api/planets
    3.5-Delete Planet. Delete call to: http://localhost:3000/api/planets/id
4- Test
   4.1- Create Test Case each functionality(see file apiTest.js)
   4.2- Run test with command npm test