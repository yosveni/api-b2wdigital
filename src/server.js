const express = require('express');
const bodyParser = require('body-parser');
const app = express();


//settings
app.set('port',process.env.PORT ||3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));


//Allow all requests from all domains & localhost
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type,     Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET");
    next();
  });

//Router
require('./app/controllers/PlanetaConrtoller')(app);

//start server
app.listen(app.get('port'),()=>{
    console.log('Server on',app.get('port'));
});

module.exports = app