let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = require('chai').expect;
let app = require('../../server');

chai.use(chaiHttp);

let url= 'http://localhost:3000/api';
resultData = [];

//Test function new Planet
describe('Insert a planet: ',()=>{

	it('should insert a planet', (done) => {
		chai.request(url)
			.post('/planets')
			.send({name: "Hoth", clima: "frozen", terreno: "tundra, ice caves, mountain ranges",resultData})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});


//Test function get all planets
describe('get all planets: ',()=>{

	it('get all planets', (done) => {
		chai.request(url)
			.get('/planets')
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});

});

//Test function get planet by id
describe('Get planets by id: ',()=>{

	it('should Get planets by id', (done) => {
		chai.request(url)
			.get('/planets/5c194ab847d1d13d35866360')//pass Id
			.end( function(err,res){
				console.log(res.params)
				expect(res.body).to.have.property('_id').to.be.equal("5c194ab847d1d13d35866360");
				expect(res).to.have.status(200);
				done();
			});
	});

});


//Test function get planet by name
describe('Get planets by name: ',()=>{

	it('should Get planets by name', (done) => {
		chai.request(url)
			.get('/planets/name/Hoth')//pass name
			.end( function(err,res){
				console.log(res.params)
				expect(res.body).to.have.property('name').to.be.equal("Hoth");
				expect(res).to.have.status(200);
				done();
			});
	});

});

//Test delete planet
describe('delete the planet by id pass for params: ',()=>{

	it('should the planet by id pass for params', (done) => {
		chai.request(url)
			.get('/planets')
			.end( function(err,res){
				console.log(res.body)
				expect(res.body).to.have.lengthOf(1);
				expect(res).to.have.status(200);
				chai.request(url)
					.del('/planets/5c1859b31ae7b329eaf935a1')//pass  id of the Planet to delete
					.end( function(err,res){
						console.log(res.body)
						expect(res).to.have.status(200);
						chai.request(url)
							.get('/planets')
							.end( function(err,res){
								console.log(res.body)
								expect(res.body).to.have.lengthOf(1);
								expect(res.body[0]).to.have.property('_id').to.be.equal("5c1859b31ae7b329eaf935a1");
								expect(res).to.have.status(200);
								done();
						});
					});
			});
	});

});
