const mongoose = require('../../db');


const PlantaSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        unique:true
    },
    clima:{
        type:String,
        required:true,
    },
    terreno:{
        type:String,
        required:true,
    },
    film:{
        type:Number,
        required:false
    },
    createAt:{
        type:Date,
        default:Date.now
    }
});



const Planeta = mongoose.model('Planeta',PlantaSchema);

//export module
module.exports = Planeta