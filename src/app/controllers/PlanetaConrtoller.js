const express = require('express');
const request = require('request');

//import the model
const Planeta =  require('../models/planeta');

//Define the router
const router = express.Router();


//get all planeta
router.get('/planets',(req,res)=>{
    Planeta.find(function(err,planets){
        if(err)
            res.send(err);
        res.json(planets)
    });
});

//find by name
 router.get('/planets/name/:name',(req,res)=>{
   Planeta.findOne({name: req.params.name}, function(err,planet){
        if(err) return res.status(500).send(err)
        return res.status(200).send(planet);        

    });

}); 

//find by id
router.get('/planets/:id',(req,res)=>{
    Planeta.findById(req.params.id, function(err, planet) {
        if (err) return res.status(500).send(err)
        return res.status(200).send(planet);
    });

});


//Get data API https://swapi.co/api
request("https://swapi.co/api/planets", function (err, res, body) {
    if (err) {
      console.log(err);
      return;
    }
    requestResult = JSON.parse(body); // All Planets 
    resultData = requestResult['results'];
  });

//Create New Planeta
router.post('/planets',(req,res)=>{
    var flag = false;
    var cant = 0;   //store count films 
    for(var i = 0;i<resultData.length && !flag;i++){
        if(req.body.name ==resultData[i]['name']){
            flag = true;
            cant = resultData[i]['films'].length;//get count films for instance Planet created
        }
    }

    //Planet Object with the params
    const planeta = new Planeta({
        name:req.body.name,
        clima:req.body.clima,
        terreno:req.body.terreno,
        film: cant
    });
    planeta.save(err=>{
        if (err) return res.status(500).send(err);
        return res.status(200).send(planeta);
    });
});

// Delete  Planet by id    
router.delete('/planets/:id',(req,res)=>{
    Planeta.findByIdAndRemove(req.params.id,(err,planeta)=>{
        if(err)return res.status(500).send(err);
        const response = {
            message: "Successfully deleted",
            id: planeta._id
        };
        return res.status(200).send(response);       
    });

});


module.exports = app=>app.use('/api',router);
