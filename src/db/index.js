const mongoose = require('mongoose');


//connect and mode to the DB
const options = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    autoIndex: false, 
    reconnectTries: Number.MAX_VALUE, 
    reconnectInterval: 500, 
    poolSize: 10,   
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000, 
    socketTimeoutMS: 45000, 
    family: 4 
  };
  //pass url host and options
mongoose.connect('mongodb://localhost:27017/apib2wdigital',options);
mongoose.Promise = global.Promise;

module.exports = mongoose;

